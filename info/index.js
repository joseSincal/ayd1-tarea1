const express = require('express')
const app = express()
const port = 4000

app.get('/info', (req, res) => {
  res.send({
    name: 'Jose Sincal',
    license: 201801375
  })
})

app.get('/class', (req, res) => {
    res.send({
      name: 'Analisis y Diseño de Sistemas 1',
    })
  })

app.listen(port, () => {
  console.log(`Listening on port ${port}`)
})
