const express = require("express");
const app = express();
const port = 3000;

app.use(express.json());

app.post("/add", (req, res) => {
  let num1 = req.body.n1;
  let num2 = req.body.n2;

  console.log(`API Response: ${num1} + ${num2} = ${num1 + num2}`);

  res.send({
    first_number: num1,
    second_number: num2,
    answer: num1 + num2,
  });
});

app.post("/subtract", (req, res) => {
  let num1 = req.body.min;
  let num2 = req.body.sub;

  console.log(`API Response: ${num1} - ${num2} = ${num1 - num2}`);

  res.send({
    minuend: num1,
    subtrahend: num2,
    difference: num1 - num2,
  });
});

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
